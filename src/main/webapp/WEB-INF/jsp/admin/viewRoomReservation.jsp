<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: farid
  Date: 23.10.20
  Time: 15:58
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>View User Reservation</title>
</head>
<body>

Room Id:${room.id} <br>
Room Name: ${room.name} <br>
Room Number: ${room.number} <br>
Room Color: ${room.color} <br>


<c:choose>
    <c:when test="${not empty reservationList}" >
        <table border="2px">
            <tr>
                <th>User id</th>
                <th>User Name</th>
                <th>Start Date</th>
                <th>End Date</th>
            </tr>


        <c:forEach items="${reservationList}" var="ur">
            <tr>
                <td>${ur.user.id}</td>
                <td>${ur.user.name}</td>
                <td>${ur.startDate}</td>
                <td>${ur.endDate}</td>
            </tr>


        </c:forEach>
        </table>
    </c:when>
    <c:otherwise>
        Not Data!
    </c:otherwise>
</c:choose>

</body>
</html>
