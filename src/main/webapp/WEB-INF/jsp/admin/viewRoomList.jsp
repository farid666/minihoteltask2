<%--
  Created by IntelliJ IDEA.
  User: farid
  Date: 19.10.20
  Time: 15:56
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>ROOM LIST</title>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.22/css/jquery.dataTables.min.css"/>
</head>
<body>
<a href="addRoom">ADD ROOM_____</a>  <a href="/admin/">....USERS</a>  <a href="/admin/calendar">....CALENDAR</a><br>

<table id="roomTable">
    <thead>
    <tr>
        <th>Id</th>
        <th>Name</th>
        <th>Number</th>
        <th>Action</th>
    </tr>
    </thead>
    <tbody>

    </tbody>
</table>

<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js"></script>

<script>
    $(document).ready(function() {
        $('#roomTable').DataTable( {
            "processing": true,
            "serverSide": true,
            "ajax": "getAjaxRoom"
        } );
    } );
</script>

</body>
</html>
