<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: farid
  Date: 23.10.20
  Time: 15:58
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>View User Reservation</title>
</head>
<body>

User Id:${user.id} <br>
User Name: ${user.name} <br>
User Email: ${user.email} <br>

<c:choose>
    <c:when test="${not empty userReservation}" >
        <table border="2px">
            <tr>
                <th>Room id</th>
                <th>Room Name</th>
                <th>Start Date</th>
                <th>End Date</th>
            </tr>


        <c:forEach items="${userReservation}" var="ur">
            <tr>
                <td>${ur.room.id}</td>
                <td>${ur.room.name}</td>
                <td>${ur.startDate}</td>
                <td>${ur.endDate}</td>
            </tr>


        </c:forEach>
        </table>
    </c:when>
    <c:otherwise>
        Not Data!
    </c:otherwise>
</c:choose>

</body>
</html>
