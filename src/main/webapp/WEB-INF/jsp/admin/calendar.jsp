<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: farid
  Date: 19.10.20
  Time: 15:56
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>RESERVATION LIST</title>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.22/css/jquery.dataTables.min.css"/>
</head>
<body>
<a href="addReservation">ADD RESERVATION_____</a>  <a href="viewRoomList">....ROOMS</a> <a href="/admin/">....USERS</a> <br>

<form action="/admin/getAjaxReservation" method="get">
    <select class="form-control" data-live-search="true" name="user_id">
        <option value="-1">Select</option>
        <c:forEach items="${userList}" var="ul">
            <option value="${ul.id}"
                    data-tokens="${ul.name}">${ul.name}</option>
        </c:forEach>
    </select>

    <select class="form-control" data-live-search="true" name="room_id">
        <option value="-1">Select</option>
        <c:forEach items="${roomList}" var="ul">
            <option value="${ul.id}"
                    data-tokens="${ul.name}">${ul.name}</option>
        </c:forEach>
    </select>

    <input type="text" name="start_date" />

    <input type="text" name="end_date" />

    <input type="submit" value="Search" id="search" >
</form>

<table id="reservationTable">
    <thead>
    <tr>
        <th>Reservation Id</th>
        <th>User Name</th>
        <th>Room Name</th>
        <th>Room Number</th>
        <th>Room Color</th>
        <th>Start Date</th>
        <th>End Date</th>
        <th>Action</th>
    </tr>
    </thead>
    <tbody>

    </tbody>
</table>


<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js"></script>

<script>
    $(document).ready(function() {
        $('#reservationTable').DataTable( {
            "processing": true,
            "serverSide": true,
            "ajax": "getAjaxReservation"
        } );
    } );

</script>

</body>
</html>
