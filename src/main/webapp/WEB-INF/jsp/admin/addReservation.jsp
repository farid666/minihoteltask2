<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: farid
  Date: 16.10.20
  Time: 18:48
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Add Reservation</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">

</head>
<body>
<form:form modelAttribute="addReservation" action="addReservation" method="post">
    <div class="form-group">
        <label for="exampleInputEmail1">User Name</label>
<%--        <form:input path="user.name" class="form-control" id="exampleInputEmail1" />--%>
<%--        <form:select path="user.name" id="exampleInputEmail1">--%>
<%--            <c:forEach items="${userList}" var="ul">--%>
<%--                <form:option value="${ul.name}" />--%>
<%--            </c:forEach>--%>
<%--        </form:select>--%>

        <form:select id="exampleInputEmail1" class="form-control" data-live-search="true" path="user.id">
            <option value="-1">Select</option>
            <c:forEach items="${userList}" var="ul">
                <option value="${ul.id}"
                        data-tokens="${ul.name}">${ul.name}</option>
            </c:forEach>
        </form:select>
        <form:errors path="user.id" />
    </div>
    <div class="form-group">
        <label for="exampleInputPassword1">Room Name</label>
<%--        <form:input path="room.name" class="form-control"  id="exampleInputPassword1" />--%>
<%--        <form:select path="room.name" id="exampleInputPassword1">--%>
<%--            <c:forEach items="${roomList}" var="ul">--%>
<%--                <option value="${ul.id}"--%>
<%--                        data-tokens="${ul.name}">${ul.name}</option>--%>
<%--            </c:forEach>--%>
<%--        </form:select>--%>

        <form:select id="exampleInputPassword1" class="form-control" data-live-search="true" path="room.id">
            <option value="-1">Select</option>
            <c:forEach items="${roomList}" var="ul">
                <option value="${ul.id}"
                        data-tokens="${ul.name}">${ul.name}</option>
            </c:forEach>
        </form:select>
        <form:errors path="room.id"/>
    </div>
    <div class="form-group">
        <label for="exampleInputPassword1">Start Date</label>
        <form:input path="startDate" class="form-control"  id="exampleInputPassword1" />
        <form:errors path="startDate" />
    </div>
    <div class="form-group">
        <label for="exampleInputPassword1">End Date</label>
        <form:input path="endDate"  class="form-control"  id="exampleInputPassword1" />
        <form:errors path="endDate" />
    </div>
    <button type="submit" class="btn btn-primary">ADD</button>
</form:form>
</body>
<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx" crossorigin="anonymous"></script>

</html>
