package com.hotel.service;

public interface PasswordService {
    String hashedPassword(String cleanPassword);
}
