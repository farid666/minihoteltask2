package com.hotel.service;

import org.springframework.core.io.Resource;
import org.springframework.web.multipart.MultipartFile;

public interface FileService {
    String saveFile(long userId, MultipartFile file);
    Resource getFile(String file);
}
