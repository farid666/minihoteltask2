package com.hotel.service;

import com.hotel.domain.*;

import java.util.List;
import java.util.Optional;

public interface UserService {
    DataTableResponse getUserList(DataTableRequest request);
    User addUser(UserDto userDto);
    Optional<User> getUserById(long userId);
    boolean deleteUserById(long userId);
    void editUser(EditUserDto editUserDto);
    User editUser2(EditUserDto editUserDto);
    List<User> getUserList();
    List<Reservation> getUserReservation(long userId);
    void updateUserImage(User user);
    PageResponse<User> getUserListRest(PageRequest pageRequest);
    long getUserPageCount(int size);
    Optional<User> getUserByEmail(String email);
    List<Role> getUserRoles(long userId);
}
