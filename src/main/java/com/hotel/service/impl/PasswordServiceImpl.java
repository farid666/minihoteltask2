package com.hotel.service.impl;

import com.hotel.service.PasswordService;
import org.mindrot.jbcrypt.BCrypt;
import org.springframework.stereotype.Service;

@Service
public class PasswordServiceImpl implements PasswordService {
    @Override
    public String hashedPassword(String cleanPassword) {
        return BCrypt.hashpw(cleanPassword,BCrypt.gensalt());
    }
}
