package com.hotel.service.impl;

import com.hotel.domain.*;
import com.hotel.repository.RoomRepository;
import com.hotel.service.RoomService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class RoomServiceImpl implements RoomService {

    @Autowired
    private RoomRepository roomRepository;

    @Override
    public DataTableResponse getRoomList(DataTableRequest request) {
        DataTableResponse response = new DataTableResponse();
        response.setDraw(request.getDraw());
        response.setRecordsTotal(roomRepository.getTotalRoomCount());
        response.setRecordsFiltered(roomRepository.getSearchRoomCount(request.getSearchValue()));
        List<Room> roomList = roomRepository.getRoomData(request.getSearchValue(),request.getSortColumn(),request.getSortDirection(),request.getStart(),request.getLength());
        response.setData(new Object[roomList.size()][5]);

        for (int i = 0; i < roomList.size(); i++) {
            response.getData()[i][0] = roomList.get(i).getId();
            response.getData()[i][1] = roomList.get(i).getName();
            response.getData()[i][2] = roomList.get(i).getNumber();
            response.getData()[i][3] = String.format("<a href='viewRoomReservation?id=%d'>VIEW</a> &nbsp;" +
                    "<a href='editRoom?id=%d'>EDIT</a> &nbsp;" +
                    "  <a href='deleteRoom?id=%d'>DELETE</a>", roomList.get(i).getId(),roomList.get(i).getId(),roomList.get(i).getId());
        }

        return response;
    }

    @Transactional
    @Override
    public Room addRoom(Room room) {
        return roomRepository.addRoom(room);
    }

    @Override
    public Optional<Room> getRoomById(long roomId) {
        return roomRepository.getRoomById(roomId);
    }

    @Override
    @Transactional
    public Room editRoom(Room room) {
        return roomRepository.editRoom(room);
    }

    @Override
    @Transactional
    public boolean deleteRoomById(long roomId) {
        long count = roomRepository.checkReservationByRoomId(roomId);
        if (count == 0) {
            return roomRepository.deleteRoomById(roomId);
        }else {
            System.out.println("room already reserved thats why can not delete room");
            throw new RuntimeException("room already reserved thats why can not delete room");
        }
    }

    @Override
    public List<Room> getRoomList() {
        return roomRepository.getRoomList();
    }

    @Override
    public List<Reservation> getRoomReservation(long roomId) {
        return roomRepository.getRoomReservation(roomId);
    }

    @Override
    public PageResponse<Room> getRoomListRest(PageRequest request) {
        PageResponse<Room> response = new PageResponse<>();
        response.setTotalCount(roomRepository.getTotalRoomCount());
        response.setCurrentPage(request.getPage());
        response.setPageSize(request.getSize());
        List<Room> roomList = roomRepository.getRoomData(request.getFilter(), request.getSortColumn(), request.getSortOrder(), (request.getPage() - 1) * request.getSize(), request.getSize());
        response.setData(roomList);
        response.setItemCount(roomList.size());
        response.setPageCount(getPageCount(request.getSize()));
        return response;
    }

    @Override
    public long getPageCount(int size) {
        long roomCount = roomRepository.getTotalRoomCount();

        long pageCount = roomCount / size;

        if (roomCount % size > 0) {
            pageCount++;
        }
        return pageCount;
    }
}
