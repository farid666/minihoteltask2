package com.hotel.service.impl;

import com.hotel.domain.*;
import com.hotel.repository.UserRepository;
import com.hotel.service.PasswordService;
import com.hotel.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PasswordService passwordService;

    @Override
    public DataTableResponse getUserList(DataTableRequest request) {
        DataTableResponse response = new DataTableResponse();
        response.setDraw(request.getDraw());
        response.setRecordsTotal(userRepository.getUserTotalCount());
        response.setRecordsFiltered(userRepository.getUserSearchCount(request.getSearchValue()));
        List<User> userList = userRepository.getUserData(request.getSearchValue(),request.getSortColumn(), request.getSortDirection(), request.getStart(), request.getLength());
        System.out.println("userlIst = " +userList);
        response.setData(new Object[userList.size()][4]);

        if (!userList.isEmpty()) {
            for (int i = 0; i < userList.size(); i++) {
                response.getData()[i][0] = userList.get(i).getName();
                response.getData()[i][1] = userList.get(i).getEmail();
                response.getData()[i][2] = userList.get(i).getRoleList();
                response.getData()[i][3] = String.format("<a href='viewUserReservation?id=%d'>VIEW</a> &nbsp;" +
                        "<a href='editUser?id=%d'>EDIT</a> &nbsp;" +
                        "  <a href='deleteUser?id=%d'>DELETE</a>", userList.get(i).getId(),userList.get(i).getId(),userList.get(i).getId());
            }
        }
        return response;
    }

    @Transactional
    @Override
    public User addUser(UserDto userDto) {
        String hashedPassword = passwordService.hashedPassword(userDto.getPassword());
        userDto.setPassword(hashedPassword);
        User user = new User();
        user.setName(userDto.getName());
        user.setEmail(userDto.getEmail());
        user.setPassword(userDto.getPassword());
        user.setRoleList(Collections.singletonList(Role.fromValue(userDto.getRole())));
        System.out.println("user getrolelist = " +user.getRoleList());
//        userRepository.addUserRole(userDto.getId(),userDto.getRole());
        return userRepository.addUser(user);
    }

    @Override
    public Optional<User> getUserById(long userId) {
        return userRepository.getUserById(userId);
    }

    @Override
    public boolean deleteUserById(long userId) {
        List<Reservation> list = userRepository.getUserReservation(userId);
        if (list.isEmpty()) {
            return userRepository.deleteUserById(userId);
        }else {
            throw new RuntimeException("user has resevation thats why can not delete user");
        }
    }

    @Override
    public void editUser(EditUserDto editUserDto) {
        Optional<User> optionalUser = userRepository.getUserById(editUserDto.getId());
        if (optionalUser.isPresent()) {
            User user = optionalUser.get();

            if (!user.getRoleList().contains(Role.fromValue(editUserDto.getRole()))) {
                userRepository.editUserRole(editUserDto);
            }else {
                userRepository.editUser(editUserDto);
            }
        }
    }

    @Override
    public User editUser2(EditUserDto editUserDto) {
        Optional<User> optionalUser = userRepository.getUserById(editUserDto.getId());

        if (optionalUser.isPresent()) {
            User user = optionalUser.get();

            if (!user.getRoleList().contains(Role.fromValue(editUserDto.getRole()))) {
                return userRepository.editUserRole(editUserDto);
            }else {
                return userRepository.editUser(editUserDto);
            }
        }else {
            throw new RuntimeException("user not found");
        }
    }

    @Override
    public List<User> getUserList() {
        return userRepository.getUserList();
    }

    @Override
    public List<Reservation> getUserReservation(long userId) {
        return userRepository.getUserReservation(userId);
    }

    @Override
    public void updateUserImage(User user) {
        userRepository.updateUserImage(user);
    }

    @Override
    public PageResponse<User> getUserListRest(PageRequest pageRequest) {
        PageResponse<User> response = new PageResponse<>();
        response.setTotalCount(userRepository.getUserTotalCount());
        response.setCurrentPage(pageRequest.getPage());
        response.setPageSize(response.getPageSize());
        List<User> userList = userRepository.getUserData(pageRequest.getFilter(),pageRequest.getSortColumn(), pageRequest.getSortOrder(), (pageRequest.getPage() - 1) * pageRequest.getSize(), pageRequest.getSize());
        response.setData(userList);
        response.setItemCount(userList.size());
        response.setPageCount(getUserPageCount(pageRequest.getSize()));
        return response;
    }

    @Override
    public long getUserPageCount(int size) {
        long userCount = userRepository.getUserTotalCount();
        long pageCount = userCount / size;

        if (userCount % size > 0) {
            pageCount++;
        }
        return pageCount;
    }

    @Override
    public Optional<User> getUserByEmail(String email) {
        return userRepository.getUserByEmail(email);
    }

    @Override
    public List<Role> getUserRoles(long userId) {
        return userRepository.getUserRoles(userId);
    }

}
