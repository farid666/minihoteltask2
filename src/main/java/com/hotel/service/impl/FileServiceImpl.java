package com.hotel.service.impl;

import com.hotel.service.FileService;
import com.hotel.util.FileExtension;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.FileUrlResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Service
public class FileServiceImpl implements FileService {

    @Value("${upload-dir}")
    String uploadDir;

    @Override
    public String saveFile(long userId, MultipartFile file) {
        String fileDir = "";


        try {
            if (file.getOriginalFilename().contains("..")) {
                throw new RuntimeException("File name contains unacceptiable characters");
            }

            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyyMMddHHmmss");
            // home/desktop/profileImage/userId/20200925120500.jpg

            fileDir = String.format("%s%s%s%s%s%s",uploadDir,
                    File.separatorChar,
                    userId,
                    File.separatorChar,
                    LocalDateTime.now().format(formatter),
                    FileExtension.getFileExtension(file.getOriginalFilename()));

            System.out.println("fileDir = "+ fileDir);

            Path filePath = Paths.get(fileDir);

            System.out.println("filePath = " +filePath);

            if (!Files.exists(filePath.getParent())) {
                Files.createDirectory(filePath.getParent());
            }

            System.out.println("file inputstream= "+file.getInputStream());
            Files.copy(file.getInputStream(),filePath, StandardCopyOption.REPLACE_EXISTING);

        }catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("File Upload Exception for user = " + userId);
        }

        return fileDir;
    }

    @Override
    public Resource getFile(String file) {
        Resource resource = null;
        try {
            resource = new FileUrlResource(file);
            System.out.println("resource = " +resource);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return resource;
    }
}
