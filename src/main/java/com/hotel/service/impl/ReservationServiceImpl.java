package com.hotel.service.impl;

import com.hotel.domain.*;
import com.hotel.repository.ReservationRepository;
import com.hotel.service.ReservationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Optional;

@Service
public class ReservationServiceImpl implements ReservationService {

    @Autowired
    private ReservationRepository reservationRepository;

    @Override
    public DataTableResponse getAjaxReservation(DataTableRequest request) {
        DataTableResponse response = new DataTableResponse();
        response.setDraw(request.getDraw());
        response.setRecordsTotal(reservationRepository.getTotalReservationCount());
        response.setRecordsFiltered(reservationRepository.getSearchReservationCount(request.getSearchValue()));
        List<Reservation> list = reservationRepository.getReservationData(request.getSearchValue(),request.getSortColumn(),request.getSortDirection(),request.getStart(),request.getLength(),request.getUserId(),request.getRoomId(),request.getStartDate(),request.getEndDate());
        response.setData(new Object[list.size()][8]);

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

        if (!list.isEmpty()) {
            for (int i = 0; i < list.size(); i++) {
                response.getData()[i][0] = list.get(i).getId();
                response.getData()[i][1] = list.get(i).getUser().getName();
                response.getData()[i][2] = list.get(i).getRoom().getName();
                response.getData()[i][3] = list.get(i).getRoom().getNumber();
                response.getData()[i][4] = list.get(i).getRoom().getColor();
                response.getData()[i][5] = formatter.format(list.get(i).getStartDate());
                response.getData()[i][6] = formatter.format(list.get(i).getEndDate());
                response.getData()[i][7] = String.format("<a href='editReservation?id=%d'>EDIT</a> &nbsp;" +
                        "<a href='deleteReservation?id=%d'>Delete</a>",list.get(i).getId(),list.get(i).getId());
            }
        }
        return response;
    }

    @Override
    public Reservation addReservation(Reservation reservation) {
        return reservationRepository.addReservation(reservation);
    }

    @Override
    public Optional<LocalDateTime> checkStartDate(LocalDateTime startDate, long roomId) {
        return reservationRepository.checkStartDate(startDate,roomId);
    }

    @Override
    public Optional<LocalDateTime> checkEndDate(LocalDateTime endDate,long roomId) {
        return reservationRepository.checkEndDate(endDate,roomId);
    }

    @Override
    public boolean deleteReservationById(long reservationId) {
        return reservationRepository.deleteReservationById(reservationId);
    }

    @Override
    public Optional<Reservation> getReservationById(long reservationId) {
        return reservationRepository.getReservationById(reservationId);
    }

    @Override
    public Optional<Reservation> getReservationByUserId(long userId) {
        return reservationRepository.getReservationByUserId(userId);
    }

    @Override
    public Reservation editReservation(Reservation reservation) {
        return reservationRepository.editReservation(reservation);
    }

    @Override
    public boolean deleteReservationByUserId(long userId) {
        return reservationRepository.deleteReservationByUserId(userId);
    }

    @Override
    public PageResponse<Reservation> getReservationList(PageRequest request) {
        PageResponse response = new PageResponse();
        response.setTotalCount(reservationRepository.getTotalReservationCount());
        response.setCurrentPage(request.getPage());
        response.setPageSize(request.getSize());
        List<Reservation> list = reservationRepository.getReservationRest(request.getFilter(), request.getSortColumn(), request.getSortOrder(), (request.getPage() - 1) * request.getSize(), request.getSize());
        response.setData(list);
        response.setItemCount(list.size());
        response.setPageSize(request.getSize());
        response.setPageCount(pageReservationCount(request.getSize()));
        return response;
    }

    @Override
    public long pageReservationCount(int size) {
        long resCount = reservationRepository.getTotalReservationCount();

        long pageCount = resCount / size;

        if (resCount % size > 0) {
            pageCount++;
        }
        return pageCount;
    }

    @Override
    public Reservation editReservationByUserId(Reservation reservation) {
        return reservationRepository.editReservationByUserId(reservation);
    }

    @Override
    public List<Reservation> getReservationListByUserId(long userId) {
        return reservationRepository.getReservationListByUserId(userId);
    }
}
