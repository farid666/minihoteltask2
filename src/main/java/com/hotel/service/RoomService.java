package com.hotel.service;

import com.hotel.domain.*;

import java.util.List;
import java.util.Optional;

public interface RoomService {
    DataTableResponse getRoomList(DataTableRequest request);
    Room addRoom(Room room);
    Optional<Room> getRoomById(long roomId);
    Room editRoom(Room room);
    boolean deleteRoomById(long roomId);
    List<Room> getRoomList();
    List<Reservation> getRoomReservation(long roomId);
    PageResponse<Room> getRoomListRest(PageRequest request);
    long getPageCount(int size);
}
