package com.hotel.service;

import com.hotel.domain.*;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

public interface ReservationService {
    DataTableResponse getAjaxReservation(DataTableRequest request);
    Reservation addReservation(Reservation reservation);
    Optional<LocalDateTime> checkStartDate(LocalDateTime startDate, long roomId);
    Optional<LocalDateTime> checkEndDate(LocalDateTime endDate,long roomId);
    boolean deleteReservationById(long reservationId);
    Optional<Reservation> getReservationById(long reservationId);
    Optional<Reservation> getReservationByUserId(long userId);
    Reservation editReservation(Reservation reservation);
    Reservation editReservationByUserId(Reservation reservation);
    boolean deleteReservationByUserId(long userId);
    PageResponse<Reservation> getReservationList(PageRequest request);
    long pageReservationCount(int size);
    List<Reservation> getReservationListByUserId(long userId);
}
