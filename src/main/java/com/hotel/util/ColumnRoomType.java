package com.hotel.util;

public enum ColumnRoomType {
    ID(0),
    NAME(1),
    NUMBER(2);

    private int value;

    ColumnRoomType(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    public static ColumnRoomType fromValue(long value) {
        ColumnRoomType columnType = null;

        if (value == 0) {
            columnType = ID;
        }else if (value == 1) {
            columnType = NAME;
        }else if (value == 2) {
            columnType = NUMBER;
        }

        return columnType;
    }
}
