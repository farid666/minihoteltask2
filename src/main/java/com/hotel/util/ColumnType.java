package com.hotel.util;

public enum ColumnType {
    NAME(0),
    EMAIL(1),
    ROLE(2);

    private int value;

    ColumnType(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    public static ColumnType fromValue(long value) {
        ColumnType columnType = null;

        if (value == 0) {
            columnType = NAME;
        }else if (value == 1) {
            columnType = EMAIL;
        }else if (value == 2) {
            columnType = ROLE;
        }

        return columnType;
    }
}
