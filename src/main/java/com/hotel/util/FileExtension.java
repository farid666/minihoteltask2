package com.hotel.util;

public class FileExtension {

    public static String getFileExtension(String originalFile) {
        String extension = "";

        if (originalFile != null) {
            int pos = originalFile.lastIndexOf(".");

            if (pos > 0) {
                extension = originalFile.substring(pos);
            }
        }

        return extension;
    }
}
