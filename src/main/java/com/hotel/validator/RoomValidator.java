package com.hotel.validator;

import com.hotel.domain.Room;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

@Component
public class RoomValidator implements Validator {
    @Override
    public boolean supports(Class<?> aClass) {
        return aClass.equals(Room.class);
    }

    @Override
    public void validate(Object o, Errors errors) {
        Room room = (Room) o;

        ValidationUtils.rejectIfEmptyOrWhitespace(errors,"name","name.required");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors,"number","number.required");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors,"color","color.required");
    }
}
