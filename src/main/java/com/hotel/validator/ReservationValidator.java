package com.hotel.validator;

import com.hotel.domain.Reservation;
import com.hotel.service.ReservationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import java.time.LocalDateTime;
import java.util.Optional;

@Component
public class ReservationValidator implements Validator {

    @Autowired
    private ReservationService reservationService;

    @Override
    public boolean supports(Class<?> aClass) {
        return aClass.equals(Reservation.class);
    }

    @Override
    public void validate(Object o, Errors errors) {
        Reservation reservation = (Reservation) o;

        ValidationUtils.rejectIfEmptyOrWhitespace(errors,"user.id","username.required");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors,"room.id","roomname.required");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors,"startDate","startdate.required");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors,"endDate","enddate.required");

        if (!errors.hasErrors()) {

            if (reservation.getStartDate().isAfter(reservation.getEndDate())) {
                errors.rejectValue("startDate","startDate<.endDate");
            }

            Optional<LocalDateTime> optionalStart = reservationService.checkStartDate(reservation.getStartDate(), reservation.getRoom().getId());
            if (optionalStart.isPresent()) {
                LocalDateTime dateTime = optionalStart.get();
                errors.rejectValue("startDate", "startDate.error",new Object[]{String.valueOf(dateTime)},"startDate error");
            }

            Optional<LocalDateTime> optionalEnd = reservationService.checkEndDate(reservation.getEndDate(),reservation.getRoom().getId());
            if (optionalEnd.isPresent()) {
                LocalDateTime dateTime = optionalEnd.get();
                errors.rejectValue("endDate", "endDate.error",new Object[]{String.valueOf(dateTime)},"endDate error");
            }
        }

    }
}
