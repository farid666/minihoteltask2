package com.hotel.validator;

import com.hotel.domain.EditUserDto;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

@Component
public class UserDtoValidator implements Validator {
    @Override
    public boolean supports(Class<?> aClass) {
        return aClass.equals(EditUserDto.class);
    }

    @Override
    public void validate(Object o, Errors errors) {
        EditUserDto editUserDto = (EditUserDto) o;

        ValidationUtils.rejectIfEmptyOrWhitespace(errors,"name","name.required");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors,"email","email.required");
    }
}
