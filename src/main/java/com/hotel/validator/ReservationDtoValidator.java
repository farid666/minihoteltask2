package com.hotel.validator;

import com.hotel.domain.Reservation;
import com.hotel.domain.ReservationDto;
import com.hotel.service.ReservationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Optional;

@Component
public class ReservationDtoValidator implements Validator {

    @Autowired
    private ReservationService reservationService;

    @Override
    public boolean supports(Class<?> aClass) {
        return aClass.equals(ReservationDto.class);
    }

    @Override
    public void validate(Object o, Errors errors) {
        ReservationDto reservationDto = (ReservationDto) o;

        ValidationUtils.rejectIfEmptyOrWhitespace(errors,"user.id","username.required");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors,"room.id","roomname.required");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors,"startDate","startdate.required");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors,"endDate","enddate.required");

        if (!errors.hasErrors()) {

            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

            LocalDateTime startDate = LocalDateTime.parse(reservationDto.getStartDate(),formatter);
            LocalDateTime endDate = LocalDateTime.parse(reservationDto.getEndDate(),formatter);

            Optional<LocalDateTime> optionalStart = reservationService.checkStartDate(startDate, reservationDto.getRoom().getId());
            Optional<LocalDateTime> optionalEnd = reservationService.checkEndDate(endDate,reservationDto.getRoom().getId());


            if (startDate.isAfter(endDate)) {
                errors.rejectValue("startDate","startDate<.endDate");
            }else if (optionalStart.isPresent()) {
                LocalDateTime dateTime = optionalStart.get();
                errors.rejectValue("startDate", "startDate.error",new Object[]{String.valueOf(dateTime)},"startDate error");
            }else  if (optionalEnd.isPresent()) {
                LocalDateTime dateTime = optionalEnd.get();
                errors.rejectValue("endDate", "endDate.error",new Object[]{String.valueOf(dateTime)},"endDate error");
            }






        }
    }
}
