package com.hotel.validator;

import com.hotel.domain.UserDto;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

@Component
public class UserValidator implements Validator {
    @Override
    public boolean supports(Class<?> aClass) {
        return aClass.equals(UserDto.class);
    }

    @Override
    public void validate(Object o, Errors errors) {
        UserDto userDto = (UserDto) o;

        ValidationUtils.rejectIfEmptyOrWhitespace(errors,"name","name.required");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors,"email","email.required");
        ValidationUtils.rejectIfEmptyOrWhitespace(errors,"password","password.required");

    }
}
