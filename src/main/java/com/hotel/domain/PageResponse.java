package com.hotel.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class PageResponse<T> implements Serializable {
    private static final long serialVersionUID = -7252797425683845534L;

    private long totalCount;
    private long itemCount; //bu geri gelen page gore datanin sayidi meselcun axirinci sehifede 3 eded data qayidirsa onun sayi
    private List<T> data;
    private long pageCount;
    private long currentPage;
    private long pageSize;

    public long getPageSize() {
        return pageSize;
    }

    public void setPageSize(long pageSize) {
        this.pageSize = pageSize;
    }

    public long getPageCount() {
        return pageCount;
    }

    public void setPageCount(long pageCount) {
        this.pageCount = pageCount;
    }

    public long getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(long currentPage) {
        this.currentPage = currentPage;
    }

    public PageResponse() {
        this.totalCount = 0;
        this.itemCount = 0;
        this.data = new ArrayList<>();
        this.pageCount = 0;
        this.currentPage = 1;
        this.pageSize = 10;
    }

    public long getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(long totalCount) {
        this.totalCount = totalCount;
    }

    public long getItemCount() {
        return itemCount;
    }

    public void setItemCount(long itemCount) {
        this.itemCount = itemCount;
    }

    public List<T> getData() {
        return data;
    }

    public void setData(List<T> data) {
        this.data = data;
    }
}
