package com.hotel.domain;

public enum Role {
    USER(1),
    ADMIN(2);

    private int value;

    Role(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    public static Role fromValue(int value) {
        Role role = null;

        if (value == 1) {
            role = USER;
        }else if (value == 2) {
            role = ADMIN;
        }

        return role;
    }


}
