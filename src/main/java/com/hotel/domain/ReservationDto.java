package com.hotel.domain;

public class ReservationDto {
    private long id;
    private User user;
    private Room room;
    private String startDate;
    private String endDate;

    public ReservationDto() {
        this.id = 0;
        this.user = new User();
        this.room = new Room();
        this.startDate = "";
        this.endDate = "";
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Room getRoom() {
        return room;
    }

    public void setRoom(Room room) {
        this.room = room;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    @Override
    public String toString() {
        return "ReservationDto{" +
                "id=" + id +
                ", user=" + user +
                ", room=" + room +
                ", startDate='" + startDate + '\'' +
                ", endDate='" + endDate + '\'' +
                '}';
    }
}
