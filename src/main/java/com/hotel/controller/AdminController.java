package com.hotel.controller;

import com.hotel.domain.*;
import com.hotel.service.ReservationService;
import com.hotel.service.RoomService;
import com.hotel.service.UserService;
import com.hotel.validator.ReservationDtoValidator;
import com.hotel.validator.RoomValidator;
import com.hotel.validator.UserDtoValidator;
import com.hotel.validator.UserValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping("/admin")
public class AdminController {

    private static Logger log = LoggerFactory.getLogger(AdminController.class);

    @Autowired
    private UserService userService;

    @Autowired
    private UserValidator userValidator;

    @Autowired
    private UserDtoValidator userDtoValidator;

    @Autowired
    private RoomService roomService;

    @Autowired
    private RoomValidator roomValidator;

    @Autowired
    private ReservationService reservationService;

    @Autowired
    private ReservationDtoValidator reservationDtoValidator;



    @InitBinder
    public void initBinder(WebDataBinder binder) {
        Object target = binder.getTarget();

        if (target != null) {
            if (target.getClass().equals(UserDto.class)) {
                binder.setValidator(userValidator);
            }else if (target.getClass().equals(EditUserDto.class)) {
                binder.setValidator(userDtoValidator);
            }else if (target.getClass().equals(Room.class)) {
                binder.setValidator(roomValidator);
            }else if (target.getClass().equals(ReservationDto.class)) {
                binder.setValidator(reservationDtoValidator);
            }
        }
    }

    @GetMapping("/")
    public String index() {
        return "admin/index";
    }

    @GetMapping("getAjaxUser")
    @ResponseBody
    public DataTableResponse getData(
            @RequestParam(name = "draw") int draw,
            @RequestParam(name = "start") int start,
            @RequestParam(name = "length") int length,
            @RequestParam(name = "order[0][column]") int sortColumn,
            @RequestParam(name = "order[0][dir]") String sortDirection,
            @RequestParam(name = "search[value]") String searchValue
    ) {
        DataTableRequest request = new DataTableRequest();
        request.setDraw(draw);
        request.setStart(start);
        request.setLength(length);
        request.setSortColumn(sortColumn);
        request.setSortDirection(sortDirection);
        request.setSearchValue(searchValue);
        return userService.getUserList(request);
    }

    @GetMapping("/addUser")
    public ModelAndView adduSer() {
        ModelAndView modelAndView = new ModelAndView();
        UserDto user = new UserDto();
        modelAndView.addObject("addUsers", user);
        modelAndView.setViewName("admin/addUser");
        return modelAndView;
    }

    @PostMapping("addUser")
    public String addUser(@ModelAttribute(name = "addUsers") @Validated UserDto userDto,
                          BindingResult result) {

        if (result.hasErrors()) {
            return "admin/addUser";
        } else {

            userService.addUser(userDto);
            return "admin/index";
        }
    }

    @GetMapping("/deleteUser")
    public String deleteUser(@RequestParam(name = "id") long userId) {
        try {
            userService.deleteUserById(userId);
            return "admin/index";
        } catch (Exception e) {
            return "errorView";
        }
    }

    @GetMapping("editUser")
    public ModelAndView editsUser(
            @RequestParam(name = "id") long userId
    ) {
        ModelAndView modelAndView = new ModelAndView();
        Optional<User> optionalUser = userService.getUserById(userId);

        if (optionalUser.isPresent()) {
            User user = optionalUser.get();
            EditUserDto userDto = new EditUserDto();
            userDto.setId(user.getId());
            userDto.setName(user.getName());
            userDto.setEmail(user.getEmail());
            userDto.setRole(user.getRoleList().get(0).getValue());
            modelAndView.addObject("editUser", userDto);
            modelAndView.setViewName("admin/editUser");
            log.debug("user = " +user);
        } else {
            modelAndView.setViewName("errorView");
        }
        return modelAndView;
    }

    @PostMapping("editUser")
    public String editUser(@ModelAttribute(name = "editUser") @Validated EditUserDto editUserDto,
                           BindingResult result) {

        System.out.println("user dto = " + editUserDto);
        System.out.println(result.hasErrors());

        if (!result.hasErrors()) {
            userService.editUser(editUserDto);
            return "admin/index";
        } else {
            return "admin/editUser";
        }
    }

    @GetMapping("/viewRoomList")
    public String rooms() {
        return "admin/viewRoomList";
    }

    @GetMapping("getAjaxRoom")
    @ResponseBody
    public DataTableResponse getAjaxRoom(
            @RequestParam(name = "draw") int draw,
            @RequestParam(name = "start") int start,
            @RequestParam(name = "length") int length,
            @RequestParam(name = "order[0][column]") int sortColumn,
            @RequestParam(name = "order[0][dir]") String sortDirection,
            @RequestParam(name = "search[value]") String searchValue
    ) {
        DataTableRequest request = new DataTableRequest();
        request.setDraw(draw);
        request.setStart(start);
        request.setLength(length);
        request.setSortColumn(sortColumn);
        request.setSortDirection(sortDirection);
        request.setSearchValue(searchValue);
        return roomService.getRoomList(request);
    }

    @GetMapping("addRoom")
    public ModelAndView addRoom() {
        ModelAndView modelAndView = new ModelAndView();
        Room room = new Room();
        modelAndView.addObject("addRoom",room);
        modelAndView.setViewName("admin/addRoom");
        return modelAndView;
    }

    @PostMapping("addRoom")
    public String addRooms(@ModelAttribute(name = "addRoom") @Validated Room room,
                           BindingResult result) {
        if (!result.hasErrors()) {
            roomService.addRoom(room);
            return "admin/viewRoomList";
        }else {
            return "admin/addRoom";
        }

    }

    @GetMapping("/editRoom")
    public ModelAndView editRoom(@RequestParam(name = "id") long roomId) {
        ModelAndView modelAndView = new ModelAndView();
        Optional<Room> optionalRoom = roomService.getRoomById(roomId);
        if (optionalRoom.isPresent()) {
            Room room = optionalRoom.get();
            modelAndView.addObject("editRoom",room);
            modelAndView.setViewName("admin/editRoom");
        }else {
            modelAndView.setViewName("errorView");
        }
        return modelAndView;
    }

    @PostMapping("editRoom")
    public String editsRoom(@ModelAttribute(name = "editRoom") @Validated Room room,
                            BindingResult result) {
        if (!result.hasErrors()) {
            roomService.editRoom(room);
            return "admin/viewRoomList";
        }else {
            return "admin/editRoom";
        }
    }

    @GetMapping("/deleteRoom")
    public String deleteRoom(@RequestParam(name = "id") long roomId) {
        try {
            roomService.deleteRoomById(roomId);
            return "admin/viewRoomList";
        }catch (Exception e) {
            return "errorView";
        }
    }

    @GetMapping("/calendar")
    public ModelAndView calendar() {
        ModelAndView modelAndView = new ModelAndView();
        List<User> list = userService.getUserList();
        List<Room> rooms = roomService.getRoomList();
        modelAndView.addObject("roomList",rooms);
        modelAndView.addObject("userList",list);
        modelAndView.setViewName("admin/calendar");
        return modelAndView;
    }

    @GetMapping("getAjaxReservation")
    @ResponseBody
    public DataTableResponse getAjaxReservation(
            @RequestParam(name = "draw",required = false,defaultValue = "0") int draw,
            @RequestParam(name = "start",required = false,defaultValue = "0") int start,
            @RequestParam(name = "length",required = false,defaultValue = "10") int length,
            @RequestParam(name = "order[0][column]",required = false,defaultValue = "1") int sortColumn,
            @RequestParam(name = "order[0][dir]",required = false,defaultValue = "desc") String sortDirection,
            @RequestParam(name = "search[value]",required = false,defaultValue = "") String searchValue,
            @RequestParam(name = "user_id",required = false,defaultValue = "-1") long userId,
            @RequestParam(name = "room_id",required = false,defaultValue = "-1") long roomId,
            @RequestParam(name = "start_date",required = false,defaultValue = "") String startDate,
            @RequestParam(name = "end_date",required = false,defaultValue = "") String endDate
    ) {
        DataTableRequest request = new DataTableRequest();
        request.setDraw(draw);
        request.setStart(start);
        request.setLength(length);
        request.setSortColumn(sortColumn);
        request.setSortDirection(sortDirection);
        request.setSearchValue(searchValue);

        request.setUserId(userId);
        request.setRoomId(roomId);

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        LocalDateTime startDate1 = null;
        LocalDateTime endDate1 = null;

        if (!startDate.isEmpty()) {
            startDate1 = LocalDateTime.parse(startDate,formatter);
        }

        if (!endDate.isEmpty()) {
            endDate1 = LocalDateTime.parse(endDate,formatter);
        }


        request.setStartDate(startDate1);
        request.setEndDate(endDate1);

        return reservationService.getAjaxReservation(request);
    }

    @GetMapping("/addReservation")
    public ModelAndView addReserv() {
        ModelAndView modelAndView = new ModelAndView();
        List<User> list = userService.getUserList();
        List<Room> rooms = roomService.getRoomList();
        modelAndView.addObject("roomList",rooms);
        modelAndView.addObject("userList",list);
        ReservationDto reservationDto = new ReservationDto();
        modelAndView.addObject("addReservation",reservationDto);
        modelAndView.setViewName("admin/addReservation");
        System.out.println("reservation add get = "+reservationDto);
        return modelAndView;
    }

    @PostMapping("addReservation")
    public ModelAndView addReservation(@ModelAttribute(name = "addReservation") @Validated ReservationDto reservationDto,
                                 BindingResult result) {
        ModelAndView modelAndView = new ModelAndView();
        System.out.println("reservation get 2 = " +reservationDto);
        if (!result.hasErrors()) {
            System.out.println("errors = " + result);
            Reservation reservation = new Reservation();

            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

            LocalDateTime startDate = LocalDateTime.parse(reservationDto.getStartDate(),formatter);
            LocalDateTime endDate = LocalDateTime.parse(reservationDto.getEndDate(),formatter);
            reservation.setUser(reservationDto.getUser());
            reservation.setRoom(reservationDto.getRoom());
            reservation.setStartDate(startDate);
            reservation.setEndDate(endDate);

            System.out.println("reservation add post = "+reservation);
            reservationService.addReservation(reservation);
            modelAndView.setViewName("admin/calendar");
        }else {
            modelAndView.setViewName("admin/addReservation");
        }
        return modelAndView;
    }

    @GetMapping("/deleteReservation")
    public String deleteReservation(@RequestParam(name = "id") long rsrvId) {
        try {
            reservationService.deleteReservationById(rsrvId);
            return "admin/calendar";
        }catch (Exception e) {
            return "errorView";
        }
    }

    @GetMapping("/editReservation")
    public ModelAndView editReserv(@RequestParam(name = "id") long rsrvId) {
        Optional<Reservation> optionalReservation = reservationService.getReservationById(rsrvId);
        ModelAndView modelAndView = new ModelAndView();
        if (optionalReservation.isPresent()) {
            Reservation reservation = optionalReservation.get();
            ReservationDto reservationDto = new ReservationDto();
            reservationDto.setId(reservation.getId());
            reservationDto.setUser(reservation.getUser());
            reservationDto.setRoom(reservation.getRoom());

            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
            String startDate = reservation.getStartDate().format(formatter);
            String endDate = reservation.getEndDate().format(formatter);
            reservationDto.setStartDate(startDate);
            reservationDto.setEndDate(endDate);
            List<User> list = userService.getUserList();
            List<Room> rooms = roomService.getRoomList();
            modelAndView.addObject("roomList",rooms);
            modelAndView.addObject("userList",list);
            System.out.println("reservation edit get = " +reservation);
            modelAndView.addObject("editReservation",reservationDto);
            modelAndView.setViewName("admin/editReservation");
        }else {
            modelAndView.setViewName("errorView");
        }
        return modelAndView;
    }

    @PostMapping("editReservation")
    public String editReservation(@ModelAttribute(name = "editReservation") @Validated ReservationDto reservationDto,
                                  BindingResult result) {
        if (!result.hasErrors()) {
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
            LocalDateTime startDate = LocalDateTime.parse(reservationDto.getStartDate(),formatter);
            LocalDateTime endDate = LocalDateTime.parse(reservationDto.getEndDate(),formatter);
            Reservation reservation = new Reservation();
            reservation.setId(reservationDto.getId());
            reservation.setUser(reservationDto.getUser());
            reservation.setRoom(reservationDto.getRoom());
            reservation.setStartDate(startDate);
            reservation.setEndDate(endDate);
            reservationService.editReservation(reservation);
            return "admin/calendar";
        }else {
            return "admin/editReservation";
        }
    }


    @GetMapping("/viewUserReservation")
    public ModelAndView viewUserReservation(@RequestParam(name = "id") long userId) {
        ModelAndView modelAndView = new ModelAndView();
        Optional<User> optionalUser = userService.getUserById(userId);
        if (optionalUser.isPresent()) {
            User user = optionalUser.get();
            List<Reservation> reservations = userService.getUserReservation(user.getId());
            modelAndView.addObject("user",user);
            modelAndView.addObject("userReservation",reservations);
            modelAndView.setViewName("admin/viewUserReservation");
        }else {
            modelAndView.setViewName("errorView");
        }
        return modelAndView;
    }

    @GetMapping("/viewRoomReservation")
    public ModelAndView viewRoomReservation(@RequestParam(name = "id") long roomId) {
        ModelAndView modelAndView = new ModelAndView();
        Optional<Room> optionalRoom = roomService.getRoomById(roomId);
        if (optionalRoom.isPresent()) {
            Room room = optionalRoom.get();
            List<Reservation> reservationList = roomService.getRoomReservation(room.getId());
            modelAndView.addObject("room",room);
            modelAndView.addObject("reservationList",reservationList);
            modelAndView.setViewName("admin/viewRoomReservation");
        }else {
            modelAndView.setViewName("errorView");
        }
        return modelAndView;
    }

}
