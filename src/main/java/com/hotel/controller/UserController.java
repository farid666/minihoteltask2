package com.hotel.controller;

import com.hotel.domain.Reservation;
import com.hotel.domain.ReservationDto;
import com.hotel.domain.Room;
import com.hotel.domain.User;
import com.hotel.service.FileService;
import com.hotel.service.ReservationService;
import com.hotel.service.RoomService;
import com.hotel.service.UserService;
import com.hotel.validator.ReservationDtoValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Optional;

@Controller
@RequestMapping("/user")
public class UserController {

    @Autowired
    private FileService fileService;

    @Autowired
    private UserService userService;

    @Autowired
    private RoomService roomService;

    @Autowired
    private ReservationService reservationService;

    @Autowired
    private ReservationDtoValidator reservationDtoValidator;

    @InitBinder
    public void initBinder(WebDataBinder binder) {
        Object target = binder.getTarget();

        if (target != null) {
            if (target.getClass().equals(ReservationDto.class)) {
                binder.setValidator(reservationDtoValidator);
            }
        }
    }

    @GetMapping("/")
    public String index() {
        return "user/index";
    }

    @GetMapping("/addUserReservation")
    public ModelAndView addReserv() {
        ModelAndView modelAndView = new ModelAndView();
        ReservationDto reservationDto = new ReservationDto();
        List<User> userList = userService.getUserList();
        List<Room> roomList = roomService.getRoomList();
        modelAndView.addObject("user",userList);
        modelAndView.addObject("room",roomList);
        modelAndView.addObject("addUserReservation",reservationDto);
        modelAndView.setViewName("user/addReservation");
        return modelAndView;
    }

    @PostMapping("/addUserReservation")
    public ModelAndView addReservation(@ModelAttribute("addUserReservation") @Validated ReservationDto reservationDto,
                                 BindingResult result) {
        ModelAndView modelAndView = new ModelAndView();
        if (!result.hasErrors()) {
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
            LocalDateTime startDate = LocalDateTime.parse(reservationDto.getStartDate(),formatter);
            LocalDateTime endDate = LocalDateTime.parse(reservationDto.getEndDate(),formatter);
            Reservation reservation = new Reservation();
            reservation.setUser(reservationDto.getUser());
            reservation.setRoom(reservationDto.getRoom());
            reservation.setStartDate(startDate);
            reservation.setEndDate(endDate);
            reservationService.addReservation(reservation);
            modelAndView.setViewName("user/userProfile");
        }else {
            modelAndView.setViewName("user/addReservation");
        }
        return modelAndView;
    }

    @GetMapping("/deleteUserReservation")
    public String deleteReservation(@RequestParam(name = "id") long userId) {

        try {
            reservationService.deleteReservationByUserId(userId);
            return "user/userProfile";
        }catch (Exception e) {
            return "errorView";
        }
    }

    @GetMapping("/editUserReservation")
    public ModelAndView editreservation(@RequestParam(name = "id") long userId) {
        ModelAndView modelAndView = new ModelAndView();
        Optional<Reservation> optionalReservation = reservationService.getReservationByUserId(userId);
        if (optionalReservation.isPresent()) {
            Reservation reservation = optionalReservation.get();

            ReservationDto reservationDto = new ReservationDto();
            reservationDto.setId(reservation.getId());
            reservationDto.setUser(reservation.getUser());
            reservationDto.setRoom(reservation.getRoom());
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
            String startDate = reservation.getStartDate().format(formatter);
            String endDate = reservation.getEndDate().format(formatter);
            reservationDto.setStartDate(startDate);
            reservationDto.setEndDate(endDate);
            List<User> userList = userService.getUserList();
            List<Room> roomList = roomService.getRoomList();
            modelAndView.addObject("user",userList);
            modelAndView.addObject("room",roomList);
            modelAndView.addObject("editUserReservation",reservationDto);
        }else {
            modelAndView.addObject("errorView");
        }
        return modelAndView;
    }

    @PostMapping("/editUserReservation")
    public String editUserReservation(@ModelAttribute(name = "editUserReservation") @Validated ReservationDto reservationDto,
                                      BindingResult result) {
        if (!result.hasErrors()) {
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
            LocalDateTime startDate = LocalDateTime.parse(reservationDto.getStartDate(),formatter);
            LocalDateTime endDate = LocalDateTime.parse(reservationDto.getEndDate(),formatter);
            Reservation reservation = new Reservation();
            reservation.setId(reservationDto.getId());
            reservation.setUser(reservationDto.getUser());
            reservation.setRoom(reservationDto.getRoom());
            reservation.setStartDate(startDate);
            reservation.setEndDate(endDate);
            reservationService.editReservationByUserId(reservation);
            return "user/userProfile";
        }else {
            return "user/editUserReservation";
        }
    }

    @PostMapping("/profileImage")
    public void uploadImage(@RequestParam(name = "id") long userId,
                            @RequestParam(name = "image") MultipartFile image ) {

        String file = fileService.saveFile(userId,image);

        System.out.println("image = " +file);

        User user = new User();
        user.setId(userId);
        user.setProfileImage(file);

        userService.updateUserImage(user);
    }

    @GetMapping("/profileImage")
    public Resource getImage(@RequestParam(name = "id") long userId) {

        Resource resource = null;

        Optional<User> optionalUser = userService.getUserById(userId);

        if (optionalUser.isPresent()) {
            User user = optionalUser.get();
            resource = fileService.getFile(user.getProfileImage());
        }

        return resource;
    }

    @GetMapping("/addImage")
    public String addImage() {
        return "user/addImage";
    }


}
