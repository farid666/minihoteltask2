package com.hotel.controller.rest;

import com.hotel.domain.*;
import com.hotel.service.ReservationService;
import com.hotel.service.RoomService;
import com.hotel.service.UserService;
import com.hotel.validator.ReservationDtoValidator;
import com.hotel.validator.UserDtoValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Optional;

@RestController
@RequestMapping("/rest/admin")
public class AdminRestController {

    @Autowired
    private UserService userService;

    @Autowired
    private ReservationService reservationService;

    @Autowired
    private RoomService roomService;

    @Autowired
    private ReservationDtoValidator reservationDtoValidator;

    @Autowired
    private UserDtoValidator userDtoValidator;

    @InitBinder
    public void initBinder(WebDataBinder binder) {
        Object target = binder.getTarget();

        if (target != null) {
           if (target.getClass().equals(ReservationDto.class)) {
                binder.setValidator(reservationDtoValidator);
            }else if (target.getClass().equals(EditUserDto.class)) {
               binder.setValidator(userDtoValidator);
           }
        }
    }

    @GetMapping("/calendar")
    public PageResponse<Reservation> getReservationList(
            @RequestParam(name = "page",required = false,defaultValue = "1") int page,
            @RequestParam(name = "size",required = false,defaultValue = "10") int size,
            @RequestParam(name = "sortColumn",required = false,defaultValue = "0") int sortColumn,
            @RequestParam(name = "sortOrder",required = false,defaultValue = "desc") String sortOrder,
            @RequestParam(name = "filter",required = false,defaultValue = "") String filter
    ) {
        PageRequest pageRequest = new PageRequest();
        pageRequest.setPage(page);
        pageRequest.setSize(size);
        pageRequest.setSortColumn(sortColumn);
        pageRequest.setSortOrder(sortOrder);
        pageRequest.setFilter(filter);
        return reservationService.getReservationList(pageRequest);
    }

    @GetMapping("/{id}")
    public Reservation getReservationById(@PathVariable(name = "id") long reservationId) {
        Optional<Reservation> optionalReservation = reservationService.getReservationById(reservationId);
        if (optionalReservation.isPresent()) {
            return optionalReservation.get();
        }else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

    @ResponseStatus(HttpStatus.NO_CONTENT)
    @DeleteMapping("/{id}")
    public void deleteReservation(@PathVariable(name = "id") long reservationId) {
        boolean result = reservationService.deleteReservationById(reservationId);
        if (!result) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping("/addReservation")
    public Reservation addReservation(@RequestBody @Validated ReservationDto reservationDto,
                                      BindingResult result) {
        if (!result.hasErrors()) {
            Reservation reservation = new Reservation();
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

            LocalDateTime startDate = LocalDateTime.parse(reservationDto.getStartDate(),formatter);
            LocalDateTime endDate = LocalDateTime.parse(reservationDto.getEndDate(),formatter);
            reservation.setUser(reservationDto.getUser());
            reservation.setRoom(reservationDto.getRoom());
            reservation.setStartDate(startDate);
            reservation.setEndDate(endDate);
            return reservationService.addReservation(reservation);
        }else {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN);
        }
    }

    @PutMapping("/{id}")
    public Reservation updateReservation(@PathVariable(name = "id") long reservationId,
                                         @RequestBody @Validated ReservationDto reservationDto,
                                         BindingResult result) {
        //System.out.println("result = " +result);
        if (!result.hasErrors()) {
            reservationDto.setId(reservationId);
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
            LocalDateTime startDate = LocalDateTime.parse(reservationDto.getStartDate(),formatter);
            LocalDateTime endDate = LocalDateTime.parse(reservationDto.getEndDate(),formatter);
            Reservation reservation = new Reservation();
            reservation.setId(reservationDto.getId());
            reservation.setUser(reservationDto.getUser());
            reservation.setRoom(reservationDto.getRoom());
            reservation.setStartDate(startDate);
            reservation.setEndDate(endDate);
            return reservationService.editReservation(reservation);
        }else {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN);
        }

    }


    @GetMapping("/user")
    public PageResponse<User> getUserList(
            @RequestParam(name = "page",required = false,defaultValue = "1") int page,
            @RequestParam(name = "size",required = false,defaultValue = "10") int size,
            @RequestParam(name = "sortColumn",required = false,defaultValue = "0") int sortColumn,
            @RequestParam(name = "sortOrder",required = false,defaultValue = "desc") String sortOrder,
            @RequestParam(name = "filter",required = false,defaultValue = "") String filter
    ) {
        PageRequest pageRequest = new PageRequest();
        pageRequest.setPage(page);
        pageRequest.setSize(size);
        pageRequest.setSortColumn(sortColumn);
        pageRequest.setSortOrder(sortOrder);
        pageRequest.setFilter(filter);
        return userService.getUserListRest(pageRequest);
    }

    @GetMapping("/user/{id}")
    public User getUserById(@PathVariable(name = "id") long userId) {
        Optional<User> optionalUser = userService.getUserById(userId);
        if (optionalUser.isPresent()) {
            return optionalUser.get();
        }else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

    @ResponseStatus(HttpStatus.NO_CONTENT)
    @DeleteMapping("/user/{id}")
    public void deleteUserById(@PathVariable(name = "id") long userId) {
        boolean result = userService.deleteUserById(userId);
        if (!result) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping("/addUser")
    public User addUser(@RequestBody @Validated UserDto userDto,
                        BindingResult result) {
        if (!result.hasErrors()) {
            return userService.addUser(userDto);
        }else {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN);
        }
    }

    @PutMapping("/user/{id}")
    public User editUser(@PathVariable(name = "id") long userId,
                         @RequestBody @Validated EditUserDto editUserDto,
                         BindingResult result) {
        if (!result.hasErrors()) {
             Optional<User> optionalUser = userService.getUserById(userId);
             if (optionalUser.isPresent()) {
                 User user = optionalUser.get();
                 editUserDto.setId(user.getId());
                 return userService.editUser2(editUserDto);
             }else {
                 throw new ResponseStatusException(HttpStatus.NOT_FOUND);
             }
        }else {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN);
        }
    }

    @GetMapping("/room")
    public PageResponse<Room> getRoomList(
            @RequestParam(name = "page",required = false,defaultValue = "1") int page,
            @RequestParam(name = "size",required = false,defaultValue = "10") int size,
            @RequestParam(name = "sortColumn",required = false,defaultValue = "0") int sortColumn,
            @RequestParam(name = "sortOrder",required = false,defaultValue = "desc") String sortOrder,
            @RequestParam(name = "filter",required = false,defaultValue = "") String filter
    ) {
        PageRequest pageRequest = new PageRequest();
        pageRequest.setPage(page);
        pageRequest.setSize(size);
        pageRequest.setSortColumn(sortColumn);
        pageRequest.setSortOrder(sortOrder);
        pageRequest.setFilter(filter);
        return roomService.getRoomListRest(pageRequest);
    }

    @GetMapping("/room/{id}")
    public Room getRoomById(@PathVariable(name = "id") long roomId) {
        Optional<Room> optionalRoom = roomService.getRoomById(roomId);

        if (optionalRoom.isPresent()) {
            return optionalRoom.get();
        }else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

    @ResponseStatus(HttpStatus.NO_CONTENT)
    @DeleteMapping("/room/{id}")
    public void deleteRoom(@PathVariable(name = "id") long roomId) {
        boolean result = roomService.deleteRoomById(roomId);

        if (!result) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping("/addRoom")
    public Room addRoom(@RequestBody @Validated Room room,
                        BindingResult result) {
        if (!result.hasErrors()) {
            return roomService.addRoom(room);
        }else {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN);
        }
    }

    @PutMapping("/room/{id}")
    public Room editRoom(@PathVariable(name = "id") long roomId,
                         @RequestBody @Validated Room room,
                         BindingResult result) {
        if (!result.hasErrors()) {
            Optional<Room> optionalRoom = roomService.getRoomById(roomId);
            if (optionalRoom.isPresent()) {
                Room room1 = optionalRoom.get();
                room.setId(roomId);
                return roomService.editRoom(room);
            }else {
                throw new ResponseStatusException(HttpStatus.NOT_FOUND);
            }
        }else {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN);
        }
    }


}

