package com.hotel.controller.rest;

import com.hotel.domain.*;
import com.hotel.service.FileService;
import com.hotel.service.ReservationService;
import com.hotel.service.UserService;
import com.hotel.validator.ReservationDtoValidator;
import com.hotel.validator.UserDtoValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

import javax.swing.text.html.Option;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/rest/user")
public class UserRestController {

    @Autowired
    private UserService userService;

    @Autowired
    private ReservationService reservationService;

    @Autowired
    private ReservationDtoValidator reservationDtoValidator;

    @Autowired
    private FileService fileService;

    @InitBinder
    public void initBinder(WebDataBinder binder) {
        Object target = binder.getTarget();

        if (target != null) {
            if (target.getClass().equals(ReservationDto.class)) {
                binder.setValidator(reservationDtoValidator);
            }
        }
    }

    @GetMapping("/view/{id}")
    public List<Reservation> viewUserReservationList(@PathVariable(name = "id") long userId) {
        return reservationService.getReservationListByUserId(userId);
    }

    @GetMapping("/{id}")
    public Reservation getReservationByUserId(@PathVariable(name = "id") long userId) {
        Optional<Reservation> optionalReservation = reservationService.getReservationByUserId(userId);

        if (optionalReservation.isPresent()) {
            return optionalReservation.get();
        }else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }


    @ResponseStatus(HttpStatus.NO_CONTENT)
    @DeleteMapping("/{id}")
    public void deleteReservationByUserId(@PathVariable(name = "id") long userId) {
        boolean result = reservationService.deleteReservationByUserId(userId);
        if (!result) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping("/addUserReservation/{id}")
    public Reservation addUserReservation(@PathVariable(name = "id") long userId,
                                          @RequestBody @Validated ReservationDto reservationDto,
                                          BindingResult result) {
        if (!result.hasErrors()) {
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
            LocalDateTime startDate = LocalDateTime.parse(reservationDto.getStartDate(),formatter);
            LocalDateTime endDate = LocalDateTime.parse(reservationDto.getEndDate(),formatter);
            reservationDto.getUser().setId(userId);
            Reservation reservation = new Reservation();
            reservation.getUser().setId(reservationDto.getUser().getId());
            reservation.setRoom(reservationDto.getRoom());
            reservation.setStartDate(startDate);
            reservation.setEndDate(endDate);
            return reservationService.addReservation(reservation);
        }else {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN);
        }
    }

    @PutMapping("/editUserReservation/{id}")
    public Reservation editReservationByUserId(@PathVariable(name = "id") long userId,
                                               @RequestBody @Validated ReservationDto reservationDto,
                                               BindingResult result) {
        if (result.hasErrors()) {
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
            LocalDateTime startDate = LocalDateTime.parse(reservationDto.getStartDate(),formatter);
            LocalDateTime endDate = LocalDateTime.parse(reservationDto.getEndDate(),formatter);
            reservationDto.getUser().setId(userId);
            Reservation reservation = new Reservation();
            reservation.setId(reservationDto.getId());
            reservation.getUser().setId(reservationDto.getUser().getId());
            reservation.setRoom(reservationDto.getRoom());
            reservation.setStartDate(startDate);
            reservation.setEndDate(endDate);
            return reservationService.editReservationByUserId(reservation);
        }else {
            throw new ResponseStatusException(HttpStatus.FORBIDDEN);
        }
    }

    @GetMapping("/profileImage/{id}")
    public ResponseEntity<Void> uploadImage(@PathVariable(name = "id") long userId,
                                            @RequestParam(name = "image") MultipartFile image) {
        String file = fileService.saveFile(userId,image);

        User user = new User();
        user.setId(userId);
        user.setProfileImage(file);
        userService.updateUserImage(user);
        HttpHeaders headers = new HttpHeaders();
        headers.add("Location",file);
        return new ResponseEntity<>(headers,HttpStatus.NO_CONTENT);
    }

    @PostMapping("/profileImage/{id}")
    public ResponseEntity<Resource> getImage(@PathVariable(name = "id") long userId) {
        Optional<User> optionalUser = userService.getUserById(userId);
        if (optionalUser.isPresent()) {
            try{
                User user = optionalUser.get();

                Resource resource = fileService.getFile(user.getProfileImage());
                String contentType = Files.probeContentType(Paths.get(user.getProfileImage()));
                System.out.println("content type = " +contentType);
                HttpHeaders headers = new HttpHeaders();
                headers.add("Content-Type",contentType);
                return new ResponseEntity<>(resource,headers,HttpStatus.OK);
            }catch (Exception e) {
                e.printStackTrace();
                throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR,"error getting image");
            }
        }else {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND,"candidate not found");
        }
    }
}
