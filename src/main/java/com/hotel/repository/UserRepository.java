package com.hotel.repository;

import com.hotel.domain.EditUserDto;
import com.hotel.domain.Reservation;
import com.hotel.domain.Role;
import com.hotel.domain.User;

import java.util.List;
import java.util.Optional;

public interface UserRepository {
    long getUserTotalCount();
    long getUserSearchCount(String search);
    List<User> getUserData(String search, int column, String columnDir, int start, int length);
    User addUser(User user);
    Optional<User> getUserById(long userId);
    boolean deleteUserById(long userId);
    User editUser(EditUserDto editUserDto);
    User editUserRole(EditUserDto editUserDto);
    List<User> getUserList();
    List<Reservation> getUserReservation(long userId);
    void updateUserImage(User user);
    long getUserReservationTotalCount(long userId);
    Optional<User> getUserByEmail(String email);
    List<Role> getUserRoles(long userId);
}
