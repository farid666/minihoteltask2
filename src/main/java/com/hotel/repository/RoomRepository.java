package com.hotel.repository;

import com.hotel.domain.Reservation;
import com.hotel.domain.Room;

import java.util.List;
import java.util.Optional;

public interface RoomRepository {
    long getTotalRoomCount();
    long getSearchRoomCount(String search);
    List<Room> getRoomData(String search, int column, String columnDir, int start, int length);
    Room addRoom(Room room);
    Optional<Room> getRoomById(long roomId);
    Room editRoom(Room room);
    boolean deleteRoomById(long roomId);
    long checkReservationByRoomId(long roomId);
    List<Room> getRoomList();
    List<Reservation> getRoomReservation(long roomId);
}
