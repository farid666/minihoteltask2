package com.hotel.repository;

import com.hotel.domain.Reservation;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

public interface ReservationRepository {
    long getTotalReservationCount();
    long getSearchReservationCount(String search);
    List<Reservation> getReservationData(String search, int column, String columnDir, int start, int length, long userId, long roomId,LocalDateTime startDate,LocalDateTime endDate);
    Reservation addReservation(Reservation reservation);
    Optional<LocalDateTime> checkStartDate(LocalDateTime startDate, long roomId);
    Optional<LocalDateTime> checkEndDate(LocalDateTime endDate,long roomId);
    boolean deleteReservationById(long reservationId);
    Optional<Reservation> getReservationById(long reservationId);
    Optional<Reservation> getReservationByUserId(long userId);
    Reservation editReservation(Reservation reservation);
    Reservation editReservationByUserId(Reservation reservation);
    boolean deleteReservationByUserId(long userId);
    List<Reservation> getReservationRest(String search,int column,String columnDir,int page,int size);
    List<Reservation> getReservationListByUserId(long userId);
}
