package com.hotel.repository.mapper;

import com.hotel.domain.Reservation;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;

@Component
public class RsrvRowMapper implements RowMapper<Reservation> {

    @Override
    public Reservation mapRow(ResultSet rs, int i) throws SQLException {
        Reservation reservation = new Reservation();
        reservation.setId(rs.getInt("id"));
        reservation.getUser().setId(rs.getInt("user_id"));
        reservation.getRoom().setId(rs.getInt("room_id"));
        reservation.setStartDate(rs.getTimestamp("start_date").toLocalDateTime());
        reservation.setEndDate(rs.getTimestamp("end_date").toLocalDateTime());
        return reservation;
    }
}
