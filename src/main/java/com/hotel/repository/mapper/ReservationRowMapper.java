package com.hotel.repository.mapper;

import com.hotel.domain.Reservation;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import java.sql.ResultSet;
import java.sql.SQLException;

@Component
public class ReservationRowMapper implements RowMapper<Reservation> {
    @Override
    public Reservation mapRow(ResultSet rs, int i) throws SQLException {
        /*
        trv.id as rsrv_id, " +
            "tu.id as user_id,tu.name as user_name,tu.email, " +
            "tr.id as room_id,tr.name as room_name,tr.number,tr.color, " +
            "trv.start_date,trv.end_date
         */
        Reservation reservation = new Reservation();
        reservation.setId(rs.getInt("rsrv_id"));
        if (rs.getTimestamp("start_date") != null) {
            reservation.setStartDate(rs.getTimestamp("start_date").toLocalDateTime());
        }
        if (rs.getTimestamp("end_date") != null) {
            reservation.setEndDate(rs.getTimestamp("end_date").toLocalDateTime());
        }
        reservation.getUser().setId(rs.getInt("user_id"));
        reservation.getUser().setName(rs.getString("user_name"));
        reservation.getUser().setEmail(rs.getString("email"));
        reservation.getRoom().setId(rs.getInt("room_id"));
        reservation.getRoom().setName(rs.getString("room_name"));
        reservation.getRoom().setNumber(rs.getString("number"));
        reservation.getRoom().setColor(rs.getString("color"));
        return reservation;
    }
}
