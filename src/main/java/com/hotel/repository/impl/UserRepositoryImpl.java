package com.hotel.repository.impl;

import com.hotel.domain.EditUserDto;
import com.hotel.domain.Reservation;
import com.hotel.domain.Role;
import com.hotel.domain.User;
import com.hotel.repository.UserRepository;
import com.hotel.repository.mapper.ReservationRowMapper;
import com.hotel.repository.mapper.UserRowMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

@Repository
public class UserRepositoryImpl implements UserRepository {

    @Autowired
    private NamedParameterJdbcTemplate jdbcTemplate;

    @Autowired
    private UserRowMapper userRowMapper;

    @Autowired
    private ReservationRowMapper reservationRowMapper;

    @Override
    public long getUserTotalCount() {
        return jdbcTemplate.queryForObject(SqlQuery.GET_USER_TOTAL_COUNT,new MapSqlParameterSource(),Long.class);
    }

    @Override
    public long getUserSearchCount(String search) {
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("search","%" +search+ "%");
        return jdbcTemplate.queryForObject(SqlQuery.GET_USER_SEARCH_COUNT,params,Long.class);
    }

    @Override
    public List<User> getUserData(String search, int column, String columnDir, int start, int length) {
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("search","%" +search+ "%");
        Map<Integer,String> maps = new HashMap<>();
        maps.put(0,"tu.name");
        maps.put(1,"tu.email");
        maps.put(2,"tr.name");
        //System.out.println("colimun = " + column);
        //System.out.println("map colmun = " + maps.get(column));
        String sql = String.format(SqlQuery.GET_USER_DATA, maps.get(column),columnDir,start,length);
        //System.out.println("sql =" +sql);
        return jdbcTemplate.query(sql,params,userRowMapper);
    }

    @Override
    public User addUser(User user) {
        KeyHolder keyHolder = new GeneratedKeyHolder();
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("name", user.getName());
        params.addValue("email", user.getEmail());
        params.addValue("password", user.getPassword());
        int count = jdbcTemplate.update(SqlQuery.ADD_USER,params,keyHolder);
        if (count > 0) {
            user.setId(keyHolder.getKey().longValue());
            System.out.println("User added");

            // insert user roles
            for (int i = 0; i < user.getRoleList().size(); i++) {
                MapSqlParameterSource params1 = new MapSqlParameterSource();
                params1.addValue("user_id",user.getId());
                //System.out.println("role id = " + user.getRoleList().get(i).getValue());
                params1.addValue("role_id",user.getRoleList().get(i).getValue());
                int count1 = jdbcTemplate.update(SqlQuery.ADD_USER_ROLE,params1);
                if (count1 > 0) {
                    System.out.println("added roles");
                }else {
                    throw new RuntimeException("not add roles");
                }
            }

        }else {
            throw new RuntimeException("USer Not Add");
        }
        return user;
    }

    @Override
    public Optional<User> getUserById(long userId) {
        Optional<User> optionalUser = Optional.empty();
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("user_id",userId);
        List<User> userList = jdbcTemplate.query(SqlQuery.GET_USER_BY_ID,params,userRowMapper);
        if (!userList.isEmpty()) {
            optionalUser = Optional.of(userList.get(0));
        }else {
            throw new RuntimeException("User not Found");
        }
        return optionalUser;
    }

    @Override
    public boolean deleteUserById(long userId) {
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("user_id",userId);

            int count = jdbcTemplate.update(SqlQuery.DELETE_USER_BY_ID,params);
            if (count > 0) {
                System.out.println("deleted user");
            }else {
                throw new RuntimeException("can not delete User");
            }

            return count == 1;

    }

    @Override
    public User editUser(EditUserDto editUserDto) {
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("name", editUserDto.getName());
        params.addValue("email", editUserDto.getEmail());
        params.addValue("user_id", editUserDto.getId());

        User user = new User();
        int count =jdbcTemplate.update(SqlQuery.UPDATE_USER,params);

        if (count > 0) {
            System.out.println("updated user");
        }else {
            throw new RuntimeException("can not update user");
        }

        user.setId(editUserDto.getId());
        user.setName(editUserDto.getName());
        user.setEmail(editUserDto.getEmail());
        user.setProfileImage(editUserDto.getProfileImage());
        user.setPassword(editUserDto.getPassword());
        user.setRoleList(Arrays.asList(Role.fromValue(editUserDto.getRole())));
        return user;
    }

    @Override
    public User editUserRole(EditUserDto editUserDto) {
        MapSqlParameterSource params = new MapSqlParameterSource();
        User user = new User();
        params.addValue("role_id", editUserDto.getRole());
        params.addValue("user_id", editUserDto.getId());
        int count = jdbcTemplate.update(SqlQuery.UPDATE_USER_ROLE,params);

        if (count > 0) {
            System.out.println("updated user roles");
        }else {
            throw new RuntimeException("can not updated user role");
        }


        user.setId(editUserDto.getId());
        user.setName(editUserDto.getName());
        user.setEmail(editUserDto.getEmail());
        user.setProfileImage(editUserDto.getProfileImage());
        user.setPassword(editUserDto.getPassword());
        user.setRoleList(Arrays.asList(Role.fromValue(editUserDto.getRole())));

        return user;
    }

    @Override
    public List<User> getUserList() {
        return jdbcTemplate.query(SqlQuery.GET_USER_LIST, new MapSqlParameterSource(), (rs, i) -> {
            User user = new User();
            user.setId(rs.getInt("id"));
            user.setName(rs.getString("name"));
            user.setEmail(rs.getString("email"));
            user.setPassword(rs.getString("password"));
            return user;
        });
    }

    @Override
    public List<Reservation> getUserReservation(long userId) {
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("user_id",userId);
        return jdbcTemplate.query(SqlQuery.GET_USER_RESERVATION,params,reservationRowMapper);
    }

    @Override
    public void updateUserImage(User user) {
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("user_id",user.getId());
        params.addValue("image",user.getProfileImage());
        int count = jdbcTemplate.update(SqlQuery.UPDATE_IMAGE,params);

        if (count > 0) {
            System.out.println("upload User Image");
        }else {
            throw new RuntimeException("can not upload user image");
        }
    }

    @Override
    public long getUserReservationTotalCount(long userId) {
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("user_id",userId);
        return jdbcTemplate.queryForObject(SqlQuery.GET_USER_RESERVATION_TOTAL_COUNT,params,Long.class);
    }

    @Override
    public Optional<User> getUserByEmail(String email) {
        Optional<User> optionalUser = Optional.empty();
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("email",email);
        List<User> userList = jdbcTemplate.query(SqlQuery.GET_USER_BY_EMAIL,params,userRowMapper);
        if (!userList.isEmpty()) {
            optionalUser = Optional.of(userList.get(0));
        }
        return optionalUser;
    }

    @Override
    public List<Role> getUserRoles(long userId) {
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("user_id",userId);
        return jdbcTemplate.query(SqlQuery.GET_USER_ROLES_BY_USER_ID, params, (rs, i) -> {
            Role role = Role.fromValue(rs.getInt("tr.id"));
            return role;
        });
    }
}
