package com.hotel.repository.impl;

import com.hotel.domain.Reservation;
import com.hotel.domain.Room;
import com.hotel.repository.RoomRepository;
import com.hotel.repository.mapper.ReservationRowMapper;
import com.hotel.repository.mapper.RoomRowMapper;
import com.hotel.util.ColumnRoomType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public class RoomRepositoryImpl implements RoomRepository {

    @Autowired
    private NamedParameterJdbcTemplate jdbcTemplate;

    @Autowired
    private RoomRowMapper roomRowMapper;

    @Autowired
    private ReservationRowMapper reservationRowMapper;

    @Override
    public long getTotalRoomCount() {
        return jdbcTemplate.queryForObject(SqlQuery.GET_ROOM_TOTAL_COUNT,new MapSqlParameterSource(),Long.class);
    }

    @Override
    public long getSearchRoomCount(String search) {
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("search","%" +search+ "%");
        return jdbcTemplate.queryForObject(SqlQuery.GET_SEARCH_ROOM_COUNT,params,Long.class);
    }

    @Override
    public List<Room> getRoomData(String search, int column, String columnDir, int start, int length) {
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("search","%"+search+"%");
        String sql = String.format(SqlQuery.GET_ROOM_DATA, ColumnRoomType.fromValue(column),columnDir,start,length);
        return jdbcTemplate.query(sql,params,roomRowMapper);
    }

    @Override
    public Room addRoom(Room room) {
        KeyHolder keyHolder = new GeneratedKeyHolder();
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("name",room.getName());
        params.addValue("number",room.getNumber());
        params.addValue("color",room.getColor());
        int count = jdbcTemplate.update(SqlQuery.ADD_ROOM,params,keyHolder);

        if (count > 0) {
            room.setId(keyHolder.getKey().longValue());
            System.out.println("Added Room");
        }else {
            throw new RuntimeException("can not add room");
        }
        return room;
    }

    @Override
    public Optional<Room> getRoomById(long roomId) {
        Optional<Room> optionalRoom = Optional.empty();
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("room_id",roomId);
        List<Room> rooms = jdbcTemplate.query(SqlQuery.GET_ROOM_BY_ID,params,roomRowMapper);
        if (!rooms.isEmpty()) {
            optionalRoom = Optional.of(rooms.get(0));
        }else {
            throw new RuntimeException("room not found");
        }
        return optionalRoom;
    }

    @Override
    public Room editRoom(Room room) {
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("name",room.getName());
        params.addValue("number",room.getNumber());
        params.addValue("color",room.getColor());
        params.addValue("room_id",room.getId());
        int count = jdbcTemplate.update(SqlQuery.EDIT_ROOM_BY_ID,params);
        if (count > 0) {
            System.out.println("edit room");
        }else {
            throw new RuntimeException("can not edit room");
        }
        return room;
    }

    @Override
    public long checkReservationByRoomId(long roomId) {
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("room_id",roomId);
        return jdbcTemplate.queryForObject(SqlQuery.CHECK_RESERV_BY_ROOM_ID,params,Long.class);
    }

    @Override
    public List<Room> getRoomList() {
        return jdbcTemplate.query(SqlQuery.GET_ROOM_LIST,new MapSqlParameterSource(),roomRowMapper);
    }

    @Override
    public List<Reservation> getRoomReservation(long roomId) {
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("room_id",roomId);
        return jdbcTemplate.query(SqlQuery.GET_ROOM_RESERVATION,params,reservationRowMapper);
    }

    @Override
    public boolean deleteRoomById(long roomId) {
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("room_id",roomId);
        int count = jdbcTemplate.update(SqlQuery.DELETE_ROOM_BY_ID,params);
        if (count > 0) {
            System.out.println("deleted room");
        }else {
            throw new RuntimeException("can not delete room");
        }

        return count == 1;
    }
}
