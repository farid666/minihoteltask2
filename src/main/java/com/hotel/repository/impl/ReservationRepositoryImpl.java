package com.hotel.repository.impl;

import com.hotel.domain.Reservation;
import com.hotel.repository.ReservationRepository;
import com.hotel.repository.mapper.ReservationRowMapper;
import com.hotel.repository.mapper.RsrvRowMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Repository
public class ReservationRepositoryImpl implements ReservationRepository {

    @Autowired
    private NamedParameterJdbcTemplate jdbcTemplate;

    @Autowired
    private ReservationRowMapper reservationRowMapper;

    @Autowired
    private RsrvRowMapper rsrvRowMapper;

    @Override
    public long getTotalReservationCount() {
        return jdbcTemplate.queryForObject(SqlQuery.GET_RESERV_TOTAL_COUNT,new MapSqlParameterSource(),Long.class);
    }

    @Override
    public long getSearchReservationCount(String search) {
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("search","%"+search+"%");
        return jdbcTemplate.queryForObject(SqlQuery.GET_RESERVATION_SEARCH_COUNT,params,Long.class);
    }

    @Override
    public List<Reservation> getReservationData(String search, int column, String columnDir, int start, int length, long userId, long roomId,LocalDateTime startDate,LocalDateTime endDate) {
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("search","%"+search+"%");
        Map<Integer,String> maps = new HashMap<>();
        maps.put(0,"trv.id");
        maps.put(1,"tu.name");
        maps.put(2,"tr.name");
        maps.put(3,"tr.number");
        maps.put(4,"tr.color");
        maps.put(5,"trv.start_date");
        maps.put(6,"trv.end_date");
        String sql = String.format(SqlQuery.GET_RESERVATION_DATA,maps.get(column),columnDir,start,length);

        System.out.println("sql = " +sql);

        String sql1 = "";

        if (roomId != -1) {
            sql1 = sql.replace("{and FILTER}","and tr.id = :room_id ");
            params.addValue("room_id",roomId);
            System.out.println("sql room_id = " + sql1);
        }

        if(userId != -1) {
            sql1 = sql.replace("{and FILTER}","and tu.id = :user_id ");
            params.addValue("user_id",userId);
            System.out.println("sql userId = " + sql1);
        }
        if (startDate != null && endDate != null) {
            sql1 = sql.replace("{and FILTER}","and (trv.start_date between :start_date and :end_date or trv.end_date between :start_date and :end_date)");
            params.addValue("end_date",endDate);
            params.addValue("start_date",startDate);
            System.out.println("sql end_date start_date = " + sql1);
        }else if (startDate != null && endDate == null) {
            sql1 = sql.replace("{and FILTER}","and trv.start_date >= :start_date or trv.end_date <= :start_date");
            params.addValue("start_date",startDate);
            System.out.println("sql start_date = " + sql1);
        }else if (startDate == null && endDate != null) {
            sql1 = sql.replace("{and FILTER}","and trv.end_date <= :end_date ");//startdate nulldisa onu yoxlamaga ehtiyac yoxdu onsuz startdate gelir
            params.addValue("end_date",endDate);
            System.out.println("sql end_date = " + sql1);
        }else {
            sql1 = sql.replace("{and FILTER}"," ");
        }

        System.out.println("sql1 = " + sql1);

        //System.out.println(sql);
        return jdbcTemplate.query(sql1,params,reservationRowMapper);
    }

    @Override
    public Reservation addReservation(Reservation reservation) {
        KeyHolder keyHolder = new GeneratedKeyHolder();
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("user_id",reservation.getUser().getId());
        params.addValue("room_id",reservation.getRoom().getId());
        params.addValue("start_date",reservation.getStartDate());
        params.addValue("end_date",reservation.getEndDate());
        int count = jdbcTemplate.update(SqlQuery.ADD_RESERVATION,params,keyHolder);
        if (count > 0) {
            System.out.println("Added Reservation");
            reservation.setId(keyHolder.getKey().longValue());
        }else {
            throw new RuntimeException("can not add reservation");
        }
        return reservation;
    }

    @Override
    public Optional<LocalDateTime> checkStartDate(LocalDateTime startDate, long roomId) {

        Optional<LocalDateTime> optionalLocalDateTime = Optional.empty();
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("start_date",startDate);
        params.addValue("room_id", roomId);
        List<LocalDateTime> list = jdbcTemplate.query(SqlQuery.CHECK_START_DATE, params, (rs, i) -> rs.getTimestamp("end_date").toLocalDateTime());

        if(!list.isEmpty()) {
            optionalLocalDateTime = Optional.of(list.get(0));
        }
        return optionalLocalDateTime;
    }

    @Override
    public Optional<LocalDateTime> checkEndDate(LocalDateTime endDate, long room_id) {
        Optional<LocalDateTime> optionalLocalDateTime = Optional.empty();
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("end_date",endDate);
        params.addValue("room_id",room_id);
        //return jdbcTemplate.queryForObject(SqlQuery.CHECK_END_DATE,params,Long.class);
        List<LocalDateTime> list = jdbcTemplate.query(SqlQuery.CHECK_END_DATE, params, (rs, i) -> rs.getTimestamp("start_date").toLocalDateTime());

        if(!list.isEmpty()) {
            optionalLocalDateTime = Optional.of(list.get(0));
        }
        return optionalLocalDateTime;

    }

    @Override
    public boolean deleteReservationById(long reservationId) {
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("reservation_id",reservationId);
        int count = jdbcTemplate.update(SqlQuery.DELETE_RESERVATION,params);
        return count == 1;

    }

    @Override
    public Optional<Reservation> getReservationById(long reservationId) {
        Optional<Reservation> optionalReservation = Optional.empty();
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("reservation_id",reservationId);
        List<Reservation> list = jdbcTemplate.query(SqlQuery.GET_RESERVATION_BY_ID,params,reservationRowMapper);
        if (!list.isEmpty()) {
            optionalReservation = Optional.of(list.get(0));
        }
        return optionalReservation;
    }

    @Override
    public Optional<Reservation> getReservationByUserId(long userId) {
        Optional<Reservation> optionalReservation = Optional.empty();
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("user_id",userId);
        List<Reservation> list = jdbcTemplate.query(SqlQuery.GET_RESERVATION_BY_USER_ID,params,reservationRowMapper);
        if (!list.isEmpty()) {
            optionalReservation = Optional.of(list.get(0));
        }
        return optionalReservation;
    }

    @Override
    public Reservation editReservation(Reservation reservation) {
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("reservation_id",reservation.getId());
        params.addValue("user_id",reservation.getUser().getId());
        params.addValue("room_id",reservation.getRoom().getId());
        params.addValue("start_date",reservation.getStartDate());
        params.addValue("end_date",reservation.getEndDate());
        int count = jdbcTemplate.update(SqlQuery.EDIT_RESERVATION_BY_ID,params);
        if (count > 0) {
            System.out.println("edited Reservation");
        }else {
            throw new RuntimeException("can not edit Reservation");
        }
        return reservation;
    }

    @Override
    public Reservation editReservationByUserId(Reservation reservation) {
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("user_id",reservation.getUser().getId());
        params.addValue("room_id",reservation.getRoom().getId());
        params.addValue("start_date",reservation.getStartDate());
        params.addValue("end_date",reservation.getEndDate());
        int count = jdbcTemplate.update(SqlQuery.EDIT_RESERVATION_BY_USER_ID,params);
        if (count > 0) {
            System.out.println("edited Reservation by userId");
        }else {
            throw new RuntimeException("can not edit Reservation by userId");
        }
        return reservation;
    }

    @Override
    public boolean deleteReservationByUserId(long userId) {
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("user_id",userId);
        int count = jdbcTemplate.update(SqlQuery.DELETE_RESERVATION_BY_USER_ID,params);
        if (count > 0) {
            System.out.println("deleted reservation");
        }else {
            throw new RuntimeException("can not deleted reservation");
        }
        return count == 1;
    }

    @Override
    public List<Reservation> getReservationRest(String search, int column, String columnDir, int page, int size) {
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("search","%"+search+"%");
        Map<Integer,String> maps = new HashMap<>();
        maps.put(0,"trv.id");
        maps.put(1,"tu.name");
        maps.put(2,"tr.name");
        maps.put(3,"tr.number");
        maps.put(4,"tr.color");
        maps.put(5,"trv.start_date");
        maps.put(6,"trv.end_date");

        System.out.println("maps get = " +maps.get(column));

        String sql = String.format(SqlQuery.GET_RESERVATION_REST_DATA,maps.get(column),columnDir,page,size);
        System.out.println("sql = " +sql);
        return jdbcTemplate.query(sql,params,reservationRowMapper);
    }

    @Override
    public List<Reservation> getReservationListByUserId(long userId) {
        MapSqlParameterSource params = new MapSqlParameterSource();
        params.addValue("user_id",userId);
        return jdbcTemplate.query(SqlQuery.GET_RESERVATION_BY_USER_ID,params,reservationRowMapper);
    }
}
