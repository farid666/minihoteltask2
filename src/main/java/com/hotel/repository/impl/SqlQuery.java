package com.hotel.repository.impl;

public class SqlQuery {
    public static final String GET_USER_TOTAL_COUNT = "select count(id) as count from test_users ";

    public static final String GET_USER_LIST = "select id,name,email,password from test_users ";

    public static final String GET_USER_SEARCH_COUNT = "select count(tu.id) as count " +
            "from test_users_roles tur " +
            "join test_users tu on tur.user_id = tu.id " +
            "join test_roles tr on tur.role_id = tr.id " +
            "where concat(tu.name,tu.email,tr.name) like :search ";

    public static final String GET_USER_DATA = "select tu.id as user_id,tu.name as user_name,tu.email, " +
            "tr.id as role_id,tr.name as role_name " +
            "from test_users_roles tur " +
            "join test_users tu on tur.user_id = tu.id " +
            "join test_roles tr on tur.role_id = tr.id " +
            "where concat(tu.name,tu.email,tr.name) like :search " +
            "order by %s %s " +
            "limit %d, %d ";

    public static final String ADD_USER = "insert into test_users(name,email,password) " +
            "values(:name,:email,:password) ";

    public static final String ADD_USER_ROLE ="insert into test_users_roles(user_id,role_id) " +
            "values(:user_id,:role_id) ";

    public static final String GET_USER_BY_ID = "select tu.id as user_id,tu.name as user_name,tu.email, " +
            "tr.id as role_id,tr.name as role_name " +
            "from test_users_roles tur " +
            "join test_users tu on tur.user_id = tu.id " +
            "join test_roles tr on tur.role_id = tr.id " +
            "where tu.id = :user_id ";

    public static final String GET_USER_BY_EMAIL = "select tu.id as user_id,tu.name as user_name,tu.email,tu.password, " +
            "tr.id as role_id,tr.name as role_name " +
            "from test_users_roles tur " +
            "join test_users tu on tur.user_id = tu.id " +
            "join test_roles tr on tur.role_id = tr.id " +
            "where tu.email = :email ";
    
    public static final String DELETE_USER_BY_ID = "delete from test_users where id = :user_id ";

    public static final String DELETE_USER_ROLES = "delete from test_users_roles " +
            "where user_id = :user_id ";

    public static final String UPDATE_USER = "update test_users " +
            "set name = :name, " +
            "email = :email " +
            "where id = :user_id ";

    public static final String UPDATE_USER_ROLE = "update test_users_roles " +
            "set role_id = :role_id " +
            "where user_id = :user_id ";

    public static final String GET_ROOM_TOTAL_COUNT = "select count(id) as count from test_room ";

    public static final String GET_SEARCH_ROOM_COUNT = "select count(id) as count from test_room " +
            "where concat(id,name,number) like :search ";

    public static final String GET_ROOM_DATA = "select id,name,number,color from test_room " +
            "where concat(id,name,number) like :search " +
            "order by %s %s " +
            "limit %d ,%d ";

    public static final String ADD_ROOM = "insert into test_room(name,number,color) " +
            "values(:name,:number,:color) ";

    public static final String GET_ROOM_BY_ID = "select id,name,number,color from test_room " +
            "where id = :room_id ";

    public static final String GET_ROOM_LIST = "select id,name,number,color from test_room ";

    public static final String EDIT_ROOM_BY_ID = "update test_room " +
            "set name = :name, " +
            "number = :number, " +
            "color = :color " +
            "where id = :room_id ";

    public static final String CHECK_RESERV_BY_ROOM_ID = "select count(trs.user_id) from test_room tr " +
            "join test_reservation trs on trs.room_id = tr.id " +
            "where tr.id = :room_id ";

    public static final String DELETE_ROOM_BY_ID = "delete from test_room " +
            "where id = :room_id ";

    public static final String GET_RESERV_TOTAL_COUNT = "select count(id) as count from test_reservation ";

    public static final String GET_RESERVATION_SEARCH_COUNT = "select count(trv.id) as count from test_reservation trv " +
            "join test_users tu on trv.user_id = tu.id " +
            "join test_room tr on trv.room_id = tr.id " +
            "where concat(trv.id,tu.name,tr.name,tr.number,trv.start_date,trv.end_date) like :search ";

    public static final String GET_RESERVATION_DATA = "select trv.id as rsrv_id, " +
            "tu.id as user_id,tu.name as user_name,tu.email, " +
            "tr.id as room_id,tr.name as room_name,tr.number,tr.color, " +
            "trv.start_date,trv.end_date " +
            "from test_reservation trv " +
            "join test_users tu on trv.user_id = tu.id " +
            "join test_room tr on trv.room_id = tr.id " +
            "where concat(trv.id,tu.name,tr.name,tr.number,trv.start_date,trv.end_date) like :search " +
            "{and FILTER} " +
            "order by %s %s " +
            "limit %d ,%d ";

    public static final String GET_RESERVATION_REST_DATA = "select trv.id as rsrv_id, " +
            "tu.id as user_id,tu.name as user_name,tu.email, " +
            "tr.id as room_id,tr.name as room_name,tr.number,tr.color, " +
            "trv.start_date,trv.end_date " +
            "from test_reservation trv " +
            "join test_users tu on trv.user_id = tu.id " +
            "join test_room tr on trv.room_id = tr.id " +
            "where concat(trv.id,tu.name,tr.name,tr.number,trv.start_date,trv.end_date) like :search " +
            "order by %s %s " +
            "limit %d ,%d ";

    public static final String CHECK_START_DATE = "select end_date from test_reservation " +
            "where room_id = :room_id and :start_date between start_date and end_date ";

    public static final String CHECK_END_DATE = "select start_date from test_reservation " +
            "where room_id = :room_id and :end_date between start_date and end_date ";

    public static final String ADD_RESERVATION = "insert into test_reservation(user_id,room_id,start_date,end_date) " +
            "values(:user_id,:room_id,:start_date,:end_date) ";

    public static final String DELETE_RESERVATION = "delete from test_reservation " +
            "where id = :reservation_id ";

    public static final String DELETE_RESERVATION_BY_USER_ID = "delete from test_reservation " +
            "where user_id = :user_id ";

    public static final String GET_RESERVATION_BY_ID = "select trv.id as rsrv_id, " +
            "tu.id as user_id,tu.name as user_name,tu.email, " +
            "tr.id as room_id,tr.name as room_name,tr.number,tr.color, " +
            "trv.start_date,trv.end_date " +
            "from test_reservation trv " +
            "join test_users tu on trv.user_id = tu.id " +
            "join test_room tr on trv.room_id = tr.id " +
            "where trv.id = :reservation_id ";

    public static final String GET_RESERVATION_BY_USER_ID = "select trv.id as rsrv_id, " +
            "tu.id as user_id,tu.name as user_name,tu.email, " +
            "tr.id as room_id,tr.name as room_name,tr.number,tr.color, " +
            "trv.start_date,trv.end_date " +
            "from test_reservation trv " +
            "join test_users tu on trv.user_id = tu.id " +
            "join test_room tr on trv.room_id = tr.id " +
            "where tu.id = :user_id ";

    public static final String GET_USER_RESERVATION_TOTAL_COUNT = "select count(trv.id) as count " +
            "from test_reservation trv " +
            "join test_users tu on trv.user_id = tu.id " +
            "join test_room tr on trv.room_id = tr.id " +
            "where tu.id = :user_id ";

    public static final String EDIT_RESERVATION_BY_ID = "update test_reservation " +
            "set user_id = :user_id, " +
            "room_id = :room_id, " +
            "start_date = :start_date, " +
            "end_date = :end_date " +
            "where id = :reservation_id ";

    public static final String EDIT_RESERVATION_BY_USER_ID = "update test_reservation " +
            "set room_id = :room_id, " +
            "start_date = :start_date, " +
            "end_date = :end_date " +
            "where user_id = :user_id ";


    public static final String GET_USER_RESERVATION = "select trs.id as rsrv_id,trs.start_date,trs.end_date, " +
            "trs.user_id,tu.id as us_id,tu.name as user_name,tu.email, " +
            "trs.room_id,tr.id as rm_id,tr.name as room_name,tr.number,tr.color " +
            "from test_reservation trs " +
            "join test_room tr on trs.room_id = tr.id " +
            "join test_users tu on trs.user_id = tu.id " +
            "where tu.id = :user_id ";

    public static final String GET_ROOM_RESERVATION = "select trs.id as rsrv_id,trs.start_date,trs.end_date, " +
            "trs.user_id,tu.id as us_id,tu.name as user_name,tu.email, " +
            "trs.room_id,tr.id as rm_id,tr.name as room_name,tr.number,tr.color " +
            "from test_reservation trs " +
            "join test_room tr on trs.room_id = tr.id " +
            "join test_users tu on trs.user_id = tu.id " +
            "where tr.id = :room_id ";

    public static final String UPDATE_IMAGE = "update test_users " +
            "set profile_image = :image " +
            "where id = :user_id ";

    public static final String GET_USER_ROLES_BY_USER_ID = "select tr.id, tr.name from test_users_roles tus " +
            "join test_users tu on tus.user_id = tu.id " +
            "join test_roles tr on tus.role_id = tr.id " +
            "where tu.id = :user_id ";
}
