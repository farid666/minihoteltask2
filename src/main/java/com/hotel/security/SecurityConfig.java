package com.hotel.security;

import com.hotel.domain.Role;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    //spring security haqqinda suallar ver jwt token haqqinda suallari ver cache nece olur onu oyren

    @Autowired
    private HotelUserDetailsService hotelUserDetailsService;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
//        http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS) //bunu sorus ne ucun idi mutleq birde configire siralama nece verilir..
//                .and()
//                .csrf().disable().authorizeRequests()
//                .antMatchers(HttpMethod.POST,"/rest/admin/*").hasRole(Role.ADMIN.name())
//                .antMatchers(HttpMethod.GET,"/rest/admin/*").hasRole(Role.ADMIN.name())
//                .antMatchers(HttpMethod.PUT,"/rest/admin/*").hasRole(Role.ADMIN.name())
//                .antMatchers(HttpMethod.DELETE,"/rest/admin/*").hasRole(Role.ADMIN.name())
//                .antMatchers(HttpMethod.POST,"/rest/user/*").hasAnyRole(Role.ADMIN.name(),Role.USER.name())
//                .antMatchers(HttpMethod.PUT,"/rest/user/*").hasAnyRole(Role.ADMIN.name(),Role.USER.name())
//                .antMatchers(HttpMethod.GET,"/rest/user/*").hasAnyRole(Role.ADMIN.name(),Role.USER.name())
//                .antMatchers(HttpMethod.DELETE,"/rest/user/*").hasAnyRole(Role.ADMIN.name(),Role.USER.name())
//                .and().httpBasic()
//                .and()
//                .formLogin().disable();


        http//.csrf().disable()
                .authorizeRequests()
                .antMatchers("/admin/**").hasRole("ADMIN")
                .antMatchers("/user/**").hasAnyRole("ADMIN","USER")
                .antMatchers("/login").permitAll()
                .antMatchers("/").permitAll()
                .and()
                .formLogin()
                .loginPage("/login")
                .usernameParameter("email")
                .successForwardUrl("/")
                .failureForwardUrl("/login?error");
    }


    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.authenticationProvider(getAuthenticationProvider());
    }

    @Bean
    AuthenticationProvider getAuthenticationProvider() {
        DaoAuthenticationProvider daoAuthenticationProvider = new DaoAuthenticationProvider();
        daoAuthenticationProvider.setUserDetailsService(hotelUserDetailsService);
        daoAuthenticationProvider.setPasswordEncoder(getPasswordEncoder());
        return daoAuthenticationProvider;
    }

    @Bean
    PasswordEncoder getPasswordEncoder() {
        return new BCryptPasswordEncoder();
    }
}
