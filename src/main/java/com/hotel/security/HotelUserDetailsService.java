package com.hotel.security;

import com.hotel.domain.User;
import com.hotel.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class HotelUserDetailsService implements UserDetailsService {

    @Autowired
    private UserService userService;

    @Override
    public UserDetails loadUserByUsername(String userName) throws UsernameNotFoundException {
        Optional<User> optionalUser = userService.getUserByEmail(userName);
        if (optionalUser.isPresent()) {
            User user = optionalUser.get();
            user.setRoleList(userService.getUserRoles(user.getId()));
            UserPrincipals userPrincipals = new UserPrincipals(user);
            return userPrincipals;
        }else {
            throw new UsernameNotFoundException("username = "+userName + "not found");
        }
    }
}
